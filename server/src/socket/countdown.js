/* eslint-disable no-console */
import * as data from '../data';
import {
  SECONDS_TIMER_BEFORE_START_GAME,
  SECONDS_FOR_GAME
} from '../config/socketConfig';

const countdown = async ({ io, room }) => {
  let countDownValue = SECONDS_TIMER_BEFORE_START_GAME;

  const countDownCounter = setInterval(() => {
    if (countDownValue) {
      io.to(room.name).emit('countdown', { countDownValue });
    } else {
      clearInterval(countDownCounter);
      io.to(room.name).emit('game-text', { text: room.text });
      const gameTimer = setTimeout(() => {
        clearTimeout(gameTimer);
        io.to(room.name).emit('end-game');
        data.deleteRoomByName({ roomName: room.name });
      }, 1000 * SECONDS_FOR_GAME);
    }
    countDownValue -= 1;
  }, 1000);
  io.to(room.name).emit('countdown', { countDownValue });
};

export default countdown;
