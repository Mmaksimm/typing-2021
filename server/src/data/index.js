/* eslint-disable no-console */
/* eslint-disable indent */
import texts from '../../data';
import { MAXIMUM_USERS_FOR_ONE_ROOM } from '../config/socketConfig';

let users = [];
let rooms = [];

const getUsers = () => [...users];

const getRooms = () => [...rooms];

const getRoomsForShow = () => rooms.filter(room => !room?.text === true);

const hasUser = login => users.includes(login);

const addUser = ({ login }) => { users = [...users, login]; };

const deleteUserByName = ({ username }) => {
  users = users.filter(user => user !== username);
};

const addRoom = ({ roomName, username }) => {
  const room = {
    name: roomName,
    users: [
      {
        username,
        ready: false,
        progress: 0
      }
    ]
  };

  rooms = [room, ...rooms];

  return room;
};

const getRoomUsersLimit = ({ roomName }) => rooms.some(room => {
  if (room.name !== roomName) return false;
  if (room.users.length < MAXIMUM_USERS_FOR_ONE_ROOM) return true;
  return false;
});

const addUserToRoom = ({ roomName, username }) => {
  rooms = rooms.map(room => (room.name !== roomName
    ? room
    : {
      ...room,
      users: [
        ...room.users,
        {
          username,
          ready: false,
          progress: 0,
          endGame: false
        }
      ]
    }
  ));
};

const removeUserToRoom = ({ username, roomName }) => {
  let startGame = false;
  rooms = rooms.map(room => {
    if (room.name !== roomName) return room;
    const response = {
      ...room,
      users: room.users.filter(user => user.username !== username)
    };
    if (response?.users.length > 1 && response?.users.every(({ ready }) => ready === true) && !room?.text) {
      startGame = true;
    }
    return response;
  }).filter(room => (room.users.length > 0));

  return startGame;
};

const addText = ({ roomName }) => {
  let textNumber = Math.round(texts.length * Math.random());
  textNumber = Math.max(0, textNumber);
  textNumber = Math.min(texts.length - 1, textNumber);
  const text = texts[textNumber];
  let desiredRoom = {};
  rooms = rooms.map(room => {
    if (room.name !== roomName) return room;
    desiredRoom = { ...room, text };
    return { ...room, text };
  });
  return desiredRoom;
};

const userReadySwitch = ({ username, roomName }) => {
  let startGame = false;
  const updateUsers = users => {
    const response = users.map(user => (
      user.username !== username
        ? user
        : {
          ...user,
          ready: !user.ready
        }
    ));
    startGame = (response.length > 1 && response.every(({ ready }) => ready === true));
    return response;
  };

  rooms = rooms.map(room => (room.name !== roomName
    ? room
    : {
      ...room,
      users: updateUsers(room.users)
    }
  ));
  return startGame;
};

const addLetter = ({ roomName, userName }) => {
  let endGame = false;
  rooms = rooms.map(room => {
    if (roomName !== room.name) return room;
    const response = {
      ...room,
      users: room.users.map(user => {
        let endGame = false;
        if (user.username !== userName) return user;
        const progress = user.progress + 1;
        if (progress === room.text.length) {
          endGame = true;
        } else {
          endGame = false;
        }
        return { ...user, progress, endGame };
      })
    };

    endGame = response.users.every(({ endGame }) => endGame);
    return response;
  });

  return endGame;
};

const deleteRoomByName = ({ roomName }) => {
  rooms = rooms.filter(({ name }) => name !== roomName);
};

const allUsersInRoomNotReady = (({ roomName }) => {
  rooms = rooms.map(room => {
    if (roomName !== room.name) return room;
    return {
      ...room,
      users: room.users.map(user => ({ ...user, ready: false, progress: 0 }))
    };
  });
});

export {
  hasUser,
  getUsers,
  getRooms,
  getRoomsForShow,
  addUser,
  deleteUserByName,
  addRoom,
  getRoomUsersLimit,
  addUserToRoom,
  removeUserToRoom,
  addText,
  userReadySwitch,
  addLetter,
  deleteRoomByName,
  allUsersInRoomNotReady
};
