import path from 'path';

export const STATIC_PATH = {
  development: path.join(__dirname, '../../build/public'),
  production: path.join(__dirname, '../public')
};

export const PORT = 3002;
