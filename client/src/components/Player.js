import { storages } from '../constant';

const Player = ({ ready, username: user }) => {
  const username = sessionStorage.getItem(storages.username);
  return (`
  <li class="list-group-item mt-2" data-user="${user}">
    <h4 class="py-2 fs-6">
      <span class="fs-6"  class="${JSON.parse(ready) ? 'ready-status-red' : 'ready-status-green'}">
        ${JSON.parse(ready) ? '&#128994;' : '&#128308;'}
        </span > ${user}
      <i class="text-danger">${user === username ? 'you' : ''}</i>
    </h4 >
  <div class="progress user-progress-${username}">
    <div class="progress-bar" data-user="${user}"></div>
  </div>
  </li > `);
};

export default Player;
