/* eslint-disable no-console */
import Player from './Player';
import { events } from '../constant';
import * as data from '../service/data';

const PlayerList = ({ username, users }) => {
  const name = window.location.hash.split('-')[1];
  const Players = users
    .sort((userA, userB) => {
      if (userB.username === username) return 1;
      if (userA.username === username) return -1;
      if (userA.username > userB.username) return 1;
      return -1;
    })
    .map(user => Player(user)).join('');

  return `
    <div class="col-md-4">
        <h1 class="fs-3">${name}</h1>
        ${data.getTextLenght() ? '' : `
        <button type="button" class="btn btn-secondary" data-event="${events.leaveRoom}">
           &#60; Back To Rooms
        </button>`}
      <ul class="list-group list-group-flush mt-4">
        ${Players}
      </ul>
    </div>`;
};

export default PlayerList;
