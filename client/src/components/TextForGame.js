/* eslint-disable max-len */
import * as data from '../service/data';

const TextForGame = ({ progress = 0 }) => {
  const { text } = data.getRoom();
  const textLength = data.getTextLenght();

  return `
    <span class="fs-1 font-weight">
      <span class="bg-success">
      ${text.slice(0, progress)}</span><span
       class="bg-warning">${(progress < textLength) ? text[progress] : ''}</span>${(progress < textLength - 1) ? text.slice(progress + 1) : ''}
    </span > `;
};

export default TextForGame;
