import * as data from '../service/data';

const GameResults = () => {
  const { users } = data.getRoom();
  const rating = data.getRating();

  const resultList = rating.map(username => `<li class="list-group-item fs-3">${username}</li>`).join('')
    + users
      .filter(user => !rating.some(userName => userName === user.username))
      .sort((userA, userB) => userB.progress - userA.progress)
      .map(({ username }) => `<li class="list-group-item fs-3">${username}</li>`).join('');

  data.deleteRating();

  return `
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h1 class="modal-title">Game Results</h1>
          <a href="#room-${data.getRoom().name}" class="btn-close" aria-label="Close"  id="quit-results-btn"></a>
        </div>
        <div class="modal-body">
          <ol class="list-group list-group-numbered">
            ${resultList}
          </ol>
        </div>
        <div class="modal-footer">
          <a href="#room-${data.getRoom().name}" class="btn btn-light">Close</a>
        </div>
      </div>
    </div>`;
};

export default GameResults;
