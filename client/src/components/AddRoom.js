const AddRoom = `
  <div class="row py-3">
    <div class="col-md">
    <h2>Join Room Or Create New</h2>
  </div>
  <div class="row py-3">
    <div class="col-md-3">
      <a href="#new" class="btn btn-secondary btn-lg" id="quit-room-btn">Create Room</a>
    </div>
  </div>`;

export default AddRoom;
