/* eslint-disable max-len */
import { events } from '../constant';

const PlayFieldPreview = ready => `
  <div class="timer-body col-md-8 border rounded h-100 d-flex justify-content-center align-items-center">
    <div id="play-field">
      ${ready
    ? `<button type="button" class="btn btn-secondary btn-lg" data-event=${events.ready} id="ready-btn">
        Not Ready
      </button>`
    : `<button type="button" class="btn btn-secondary btn-lg" data-event=${events.ready}>
      Ready
      </button>`}
    </div>
    <div id="timer" class="timer"></div>
  </div`;

export default PlayFieldPreview;
