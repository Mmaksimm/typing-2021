import * as data from '../service/data';
import PlayFieldPreview from './PlayFieldPreview';
import PlayerList from './PlayerList';

const Game = () => {
  const { users } = data.getRoom();
  const username = data.getUsername();
  const ready = users.some(user => (user.ready && user.username === username));

  return `
      <div class="container-md h-100">
        <div class="row p-4 h-100">
          ${PlayerList({ username, users })}
          ${PlayFieldPreview(ready)}
        </div>
      </div>`;
};

export default Game;
