export const path = {
  login: '#login',
  rooms: '#rooms',
  room: '#room',
  new: '#new',
  cancel: '#cancel',
  rating: '#rating'
};

export const events = {
  login: 'login',
  createRoom: 'create-room',
  start: 'start-game',
  leaveRoom: 'leave-room',
  ready: 'ready'
};

export const storages = {
  username: 'username'
};

export const player = {
  ready: '&#128994;',
  notReady: '&#128308;'
};
