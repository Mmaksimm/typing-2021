/* eslint-disable no-unused-vars */
/* eslint-disable no-shadow */
import Game from '../components/Game';
import GameResults from '../components/GameResults';
import Login from '../components/Login';
import TextForGame from '../components/TextForGame';
import RoomsPreview from '../components/RoomsPreview';
import NewRoomName from '../components/NewRoomName';
import Countdown from '../components/Countdown';
import SecondsLeft from '../components/SecondsLeft';
import * as data from './data';

const usersProgressLink = {};
const root = document.getElementById('root');

const playField = element => {
  const field = document.getElementById('play-field');
  if (field) field.innerHTML = element;
};

const game = () => {
  root.innerHTML = Game();
};

const gameResults = () => {
  root.innerHTML = GameResults();
};

const login = () => {
  root.innerHTML = Login;
};

const roomsPreview = () => {
  const rooms = data.getRooms();
  root.innerHTML = RoomsPreview(rooms);
};

const newRoomName = () => {
  root.innerHTML = NewRoomName;
};

const countDown = ({ countDownValue }) => {
  playField(Countdown(countDownValue));
};

const textForGame = (progress = 0) => {
  playField(TextForGame(progress));
};

const setUsersProgressLink = () => {
  const usersProgress = document.getElementsByClassName('progress-bar');

  for (let i = 0; i < usersProgress.length; i += 1) {
    const username = usersProgress[i].dataset.user;
    usersProgressLink[username] = usersProgress[i];
  }
};

const setUserProgressByName = ({ username, progress }) => {
  const textLength = data.getTextLenght();
  if (!usersProgressLink[username]) return;
  usersProgressLink[username].style.width = `${(100 * progress) / textLength}%`;
  if (progress !== textLength) return;
  usersProgressLink[username].style.backgroundColor = '#0fe683';
};

const deleteUserProgressLinkByName = ({ username }) => {
  delete usersProgressLink[username];
};

const secondsLeft = () => {
  const timerPlace = document.getElementById('timer');
  const SECONDS_FOR_GAME = 60;
  let count = SECONDS_FOR_GAME;

  timerPlace.innerHTML = SecondsLeft(count);
  count -= 1;

  const timer = setInterval(() => {
    timerPlace.innerHTML = SecondsLeft(count);
    if (count === 0) clearInterval(timer);
    count -= 1;
  }, 1000);
};

export {
  game,
  gameResults,
  login,
  textForGame,
  newRoomName,
  roomsPreview,
  countDown,
  setUsersProgressLink,
  setUserProgressByName,
  deleteUserProgressLinkByName,
  secondsLeft
};
