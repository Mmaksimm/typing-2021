import { path } from '../constant';
import * as render from './render';
import * as roomService from './roomService';

const router = async () => {
  const { location: { hash } } = window;

  switch (hash) {
    case path.login:
      render.login();
      break;

    case path.new:
      render.newRoomName();
      break;

    case path.rooms:
      roomService.roomsPreview();
      break;

    case path.rating:
      roomService.gameResults();
      break;

    default:
      await render.game();
  }
};

export default router;
