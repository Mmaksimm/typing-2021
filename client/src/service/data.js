/* eslint-disable no-shadow */
import { storages } from '../constant';

let username = '';
let rooms = [];
let room = {};
let textLength = 0;
let rating = [];

const getUsername = () => {
  if (username) return username;
  username = sessionStorage.getItem(storages.username) || false;
  return username;
};

const setUsername = async userName => {
  username = userName;
  await sessionStorage.setItem(storages.username, userName);
};

const getRooms = () => rooms;

const setRooms = getRooms => {
  rooms = [...getRooms];
};

const getRoom = () => ({ ...room });

const getRoomByName = ({ name }) => rooms.find(room => (room.name === name)) || false;

const leaveForRoom = ({ userName }) => {
  room = {
    ...room,
    users: room.users.filter(({ username }) => username !== userName)
  };
};

const setRoom = ({ name = false }) => {
  if (!name) {
    room = {};
    return;
  }
  room = getRoomByName({ name });
};

const addRoom = newRoom => {
  rooms = [newRoom, ...rooms];
};

const createRoom = ({ name }) => {
  const newRoom = {
    name,
    game: false,
    users: [
      {
        username,
        ready: false,
        progress: 0
      }
    ]
  };

  room = { ...newRoom };
  rooms = [...rooms, newRoom];
};

const joinRoom = ({ name, userName = username }) => {
  rooms = rooms.map(room => (room.name !== name
    ? room
    : {
      ...room,
      users: [
        ...room.users,
        {
          username: userName,
          ready: false,
          progress: 0
        }
      ]
    }
  ));

  if (getRoom().name === name) setRoom({ name });
};

const leaveRoom = ({ name = room?.name, userName = username }) => {
  rooms = rooms.map(room => (room.name !== name
    ? room
    : {
      ...room,
      users: room.users.filter(user => user.username !== userName)
    }
  )).filter(({ users }) => (users.length > 0));

  if (userName === username) setRoom({});
  if (name === room.name && userName !== username) leaveForRoom({ userName });
  if (room?.users?.every(({ ready }) => ready)) textLength = 1;
};

const deleteRoomByName = ({ name = room.name }) => {
  rooms = rooms.filter(room => room.name !== name);
};

const readySwitch = ({ name = room.name, userName = username }) => {
  const updateUsers = users => users.map(user => (user.username !== userName
    ? user
    : {
      ...user,
      ready: !user.ready
    }
  ));

  const updateRoom = room => ({
    ...room,
    users: updateUsers(room.users)
  });

  rooms = rooms.map(room => (room.name !== name
    ? room
    : updateRoom(room)
  ));

  if (room?.name === name) {
    room = updateRoom(room);
  }

  if (room?.users?.every(({ ready }) => ready)) textLength = 1;
};

const setRoomText = ({ text }) => {
  room = { ...room, text };
  textLength = text?.length;
};

const setProgress = ({ key = false }) => {
  const { progress } = room.users.find(user => username === user.username);
  if (room.text[progress] !== key) return false;
  room = {
    ...room,
    users: room.users.map(user => (user.username !== username

      ? user
      : { ...user, progress: progress + 1 }
    ))
  };
  return progress + 1;
};

const addProgress = ({ username }) => {
  let progress = 0;
  room = {
    ...room,
    users: room.users.map(user => {
      if (user.username !== username) return user;
      progress = user.progress + 1;
      return { ...user, progress };
    })
  };
  return progress;
};

const getTextLenght = () => textLength;

const allUsersInRoomNotReady = () => {
  textLength = 0;
  room = {
    ...room,
    users: room.users.map(user => ({ ...user, ready: false }))
  };
};

const allUsersInRoomNotProgress = () => {
  textLength = 0;
  room = {
    ...room,
    users: room.users.map(user => ({ ...user, progress: 0 }))
  };
};

const addRating = (userName = username) => { rating = [...rating, userName]; };
const deleteRating = () => { rating = []; };
const getRating = () => [...rating];

export {
  getUsername,
  setUsername,
  getRooms,
  setRooms,
  getRoom,
  setRoom,
  addRoom,
  createRoom,
  joinRoom,
  leaveRoom,
  readySwitch,
  deleteRoomByName,
  setRoomText,
  setProgress,
  addProgress,
  getTextLenght,
  allUsersInRoomNotReady,
  addRating,
  deleteRating,
  getRating,
  allUsersInRoomNotProgress
};
