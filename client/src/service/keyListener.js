/* eslint-disable default-case */
/* eslint-disable consistent-return */
import * as data from './data';
import { path } from '../constant';
import login from '../helpers/login';
import * as roomSevice from './roomService';

const keyListener = ({ key }) => {
  if (data?.getRoom()?.text?.length) return roomSevice.gameHandler({ key });
  if (key !== 'Enter') return;
  switch (window.location.hash) {
    case path.login:
      login();
      break;

    case path.new:
      roomSevice.createRoom();
      break;
  }
};

export default keyListener;
