import * as data from './data';
import { path } from '../constant';
import socket from '../helpers/socket';
import * as render from './render';

const username = data.getUsername();

const updateRooms = async () => {
  if (window.location.hash !== path.rooms) return;
  render.roomsPreview();
};

const updateRoom = async () => {
  if (window.location.hash.split('-')[0] !== path.room) return;
  render.game();
};

const roomsPreview = async () => {
  if (!data.getUsername()) {
    window.location.replace(path.login);
    return;
  }

  const rooms = data.getRooms();

  if (!rooms) {
    await socket.emit('rooms', ({}), async (error, { rooms }) => {
      if (error) {
        alert(error.message);
        return;
      }
      render.roomsPreview(rooms);
    });
  } else {
    render.roomsPreview(rooms);
  }
};

const createRoom = async () => {
  try {
    const newRoomName = document.getElementsByTagName('input')[0].value.trim();

    if (newRoomName) {
      socket
        .emit('new-room',
          ({ room: newRoomName }),
          (error, { response }) => {
            if (error) {
              alert(error.message);
              window.location.assign(path.rooms);
            }

            if (!response) {
              alert(`The room ${newRoomName} already exists. Please choose another room name.`);
              return;
            }

            data.createRoom({ name: newRoomName });
            window.location.assign(`${path.room}-${newRoomName}`);
          });
    }
  } catch (err) {
    alert(err.message);
    window.location.assign(path.new);
  }
};

const socketCreateRoom = ({ room }) => {
  data.addRoom(room);
  updateRooms();
};

const joinRoom = async roomName => {
  try {
    socket.emit('join', ({ roomName }), error => {
      if (error) {
        alert(error.message);
        return;
      }
      data.joinRoom({ name: roomName });
      data.setRoom({ name: roomName });
      window.location.assign(`${path.room}-${roomName}`);
    });
  } catch (err) {
    alert(err.message);
  }
};

const socketUserJoin = ({ username: userName, name }) => {
  data.joinRoom({ name, userName });
  const { name: roomName } = data.getRoom() || false;
  if (roomName && roomName === name) updateRoom();
  if (!roomName) updateRooms();
};

const leaveRoom = () => {
  socket.emit('leave', async (error, { startGame }) => {
    if (error) {
      alert(error.message);
      return;
    }
    if (startGame) {
      data.deleteRoomByName({});
      updateRooms();
    }
    data.leaveRoom({});
    window.location.assign(path.rooms);
  });
};

const socketUserLeave = async ({ name, startGame, username: userName }) => {
  await data.leaveRoom({ name, userName });
  const { name: roomName } = data.getRoom() || false;
  if (roomName && roomName === name) {
    document.querySelector(`li[data-user="${userName}"]`).remove();
  }
  if (startGame) {
    data.deleteRoomByName({ name });
  }
  updateRooms();
};

const readySwitch = async () => {
  data.readySwitch({});
  updateRoom();
  socket.emit('ready-switch', (error, { startGame }) => {
    if (error) {
      alert(error.message);
      return;
    }
    if (startGame) {
      data.deleteRoomByName({});
    }
  });
};

const userReadySwitch = async ({ name, username, startGame = false }) => {
  data.readySwitch({ name, userName: username });
  const { name: roomName } = data.getRoom() || false;
  if (roomName && roomName === name) {
    updateRoom();
  }
  if (startGame) {
    data.deleteRoomByName({ name });
    updateRooms();
  }
};

const gameText = async ({ text }) => {
  if (window.location.hash.split('-')[0] !== path.room) return;
  data.setRoomText({ text });
  render.setUsersProgressLink();
  render.textForGame();
  render.secondsLeft();
};

const gameHandler = async ({ key }) => {
  const progress = data.setProgress({ key });
  if (!progress) return;
  const username = await data.getUsername();
  render.setUserProgressByName({ username, progress });
  render.textForGame({ progress });
  if (progress === data.getTextLenght()) data.addRating();
  socket.emit('letter', { username });
};

const socketAddLetter = async ({ username: userName }) => {
  if (userName === username) return;
  const progress = data.addProgress({ username: userName });
  if (progress === data.getTextLenght()) data.addRating(userName);
  render.setUserProgressByName({ progress, username: userName });
};

const socketEndGame = () => {
  socket.emit('game-end');
  data.allUsersInRoomNotReady();
  window.location.replace(path.rating);
};

const gameResults = () => {
  render.gameResults();
  data.deleteRating();
  data.allUsersInRoomNotProgress();
};

export {
  roomsPreview,
  updateRoom,
  createRoom,
  socketCreateRoom,
  joinRoom,
  socketUserJoin,
  leaveRoom,
  socketUserLeave,
  readySwitch,
  userReadySwitch,
  gameText,
  gameHandler,
  socketAddLetter,
  socketEndGame,
  gameResults
};
